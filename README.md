# Proxy Cache

This "module" creates a new cache backend to wrap around existing backend systems, with the main goal of improving troubleshooting of cache operations.

## Configuration

Configuration of this backend is very similar to other systems. The only discernible difference is that cache variables need to be set within a configuration array for Proxy Cache -

For example -

        # Add all used cache backends
        $conf['cache_backends'][] = 'sites/all/modules/proxycache/proxycache.inc';
        $conf['cache_backends'][] = 'sites/all/modules/memcache/memcache.inc';

        # Set Proxy cache as the global default backend
        $conf['cache_default_class'] = 'ProxyCache';

        # Set the "real" backends used for each cache bin
        $conf['proxycache']['cache_default_class'] = 'MemCacheDrupal';
        $conf['proxycache']['cache_class_cache_form'] = 'DrupalDatabaseCache';
