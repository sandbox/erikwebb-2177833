<?php

/**
 * @file
 * Provides a cache implementation to for troubleshooting.
 */

define('PROXYCACHE_COUNTER', 1);

define('PROXYCACHE_CACHE_FORM', 1 >> 1);

/**
 * Defines a stub cache implementation to be used for cache troubleshooting.
 */
class ProxyCache implements DrupalCacheInterface {

  /**
   * The cache bin for which the object is created.
   *
   * @var string
   */
  protected $bin;

  /**
   * The real cache backend to use.
   *
   * @var DrupalCacheInterface
   */
  protected $backend;

  /**
   * The configuration array for easy reference.
   *
   * @var array
   */
  protected $config;

  /**
   * Implements DrupalCacheInterface::__construct().
   *
   * @see _cache_get_object().
   */
  function __construct($bin) {
    $this->bin = $bin;
    $this->config = variable_get('proxycache', array());

    // Add counter output shutdown function
    if ($this->hasFlag(PROXYCACHE_COUNTER)) {
      drupal_register_shutdown_function(array('ProxyCache', 'counterStats'));
    }

    // Get real backend for the current bin.
    if (array_key_exists('cache_class_' . $bin, $this->config)) {
      $class = $this->config['cache_class_' . $bin];
    } else if (array_key_exists('cache_default_class', $this->config)) {
      $class = $this->config['cache_default_class'];
    } else {
      $class = 'DrupalDatabaseCache';
    }

    $this->backend = new $class($bin);
  }

  /**
   * Implements DrupalCacheInterface::get().
   */
  function get($cid) {
    if ($this->hasFlag(PROXYCACHE_COUNTER)) {
      $gets = &drupal_static('proxycache_gets');
      ++$gets;
    }

    return $this->backend->get($cid);
  }

  /**
   * Implements DrupalCacheInterface::getMultiple().
   */
  function getMultiple(&$cids) {
    if ($this->hasFlag(PROXYCACHE_COUNTER)) {
      $gets = &drupal_static('proxycache_gets');
      $gets += count($cids);
    }

    return $this->backend->getMultiple($cids);
  }

  /**
   * Implements DrupalCacheInterface::set().
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    if ($this->hasFlag(PROXYCACHE_COUNTER)) {
      $sets = &drupal_static('proxycache_sets');
      ++$sets;
    }

    if ($this->bin == 'cache_form' && $this->hasFlag(PROXYCACHE_CACHE_FORM)) {
      $this->debug("SET: " . $this->bin . " => " . $cid);
    }

    return $this->backend->set($cid, $data, $expire);
  }

  /**
   * Implements DrupalCacheInterface::clear().
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    $this->backend->clear($cid, $wildcard);
  }

  /**
   * Implements DrupalCacheInterface::isEmpty().
   */
  function isEmpty() {
    return $this->backend->isEmpty();
  }

  /**
   * Wrapper to pull a single configuration option.
   *
   * @param $key Configuration key to retrieve.
   */
  protected function config($key) {
    if (array_key_exists($key, $this->config) && $this->config[$key] == TRUE) {
      return $this->config[$key];
    }

    return NULL;
  }

  /**
   * Determine if a configuration flag is set.
   */
  protected function hasFlag($flag) {
    return $this->config('flags') & $flag;
  }

  protected function debug($msg) {
    if ($this->config('debug')) {
      // $backtrace = debug_backtrace();

      // Remove the current function from the backtrace
      // array_shift($backtrace);

      if (function_exists('debug')) {
        debug($msg);
      }
    }
  }

  static function counterStats() {
    if (function_exists('debug')) {
      debug("Cache sets: " . drupal_static('proxycache_sets'));
      debug("Cache gets: " . drupal_static('proxycache_gets'));
    }
  }

}
